# frozen_string_literal: true

require_relative "scout_active_support_fanout_patch/version"

module ScoutActiveSupportFanoutPatch
  # This file was ported from:
  # https://raw.githubusercontent.com/rails/rails/v7.0.4/activesupport/lib/active_support/notifications/fanout.rb
  #
  # This file is made available under the terms of the original license:
  #
  # =============================================================================
  # Copyright (c) 2005-2022 David Heinemeier Hansson
  #
  # Permission is hereby granted, free of charge, to any person obtaining
  # a copy of this software and associated documentation files (the
  # "Software"), to deal in the Software without restriction, including
  # without limitation the rights to use, copy, modify, merge, publish,
  # distribute, sublicense, and/or sell copies of the Software, and to
  # permit persons to whom the Software is furnished to do so, subject to
  # the following conditions:
  #
  # The above copyright notice and this permission notice shall be
  # included in all copies or substantial portions of the Software.
  #
  # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  # EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  # MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  # NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
  # LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
  # OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  # WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  # =============================================================================

  module ActiveSupport
    module Notifications
      class InstrumentationSubscriberError < RuntimeError
        attr_reader :exceptions

        def initialize(exceptions)
          @exceptions = exceptions
          exception_class_names = exceptions.map { |e| e.class.name }
          super "Exception(s) occurred within instrumentation subscribers: #{exception_class_names.join(', ')}"
        end
      end

      module FanoutPatch
        def start(name, id, payload)
          iterate_guarding_exceptions(listeners_for(name)) { |s| s.start(name, id, payload) }
        end

        def finish(name, id, payload, listeners = listeners_for(name))
          iterate_guarding_exceptions(listeners) { |s| s.finish(name, id, payload) }
        end

        def publish(name, *args)
          iterate_guarding_exceptions(listeners_for(name)) { |s| s.publish(name, *args) }
        end

        def publish_event(event)
          iterate_guarding_exceptions(listeners_for(event.name)) { |s| s.publish_event(event) }
        end

        # rubocop: disable Lint/RescueException
        # NOTE: Ported from rails7 which rescues Exception FSR
        def iterate_guarding_exceptions(listeners)
          exceptions = nil

          listeners.each do |s|
            yield s
          rescue Exception => e
            exceptions ||= []
            exceptions << e
          end

          if exceptions
            if exceptions.size == 1
              raise exceptions.first
            else
              raise InstrumentationSubscriberError.new(exceptions), cause: exceptions.first
            end
          end

          listeners
        end
        # rubocop: enable Lint/RescueException
      end
    end
  end
end

ActiveSupport::Notifications::Fanout.prepend ScoutActiveSupportFanoutPatch::ActiveSupport::Notifications::FanoutPatch
